Protótipo de arquitetura que pretende emular a arquitetura do projeto WiBX da Ecossistema. Chamamos este protótipo de Ping&Pong e ele será composto dos seguintes elementos:

1. Ping - um container executando **Node.JS** e uma aplicação em **JavaScript** utilizando **Seneca AMQP** - esta aplicação também se chamará *Ping*;
1. Pong - outro container semelhante ao *Ping*, porém denominado, obviamente *Pong*;
1. Mesa - um container rodando **RabbitMQ** (a princípio entendemos que o serviço de middleware do projeto WiBX roda sob **RabbitMQ** e estamos aguardando confirmação da Equipe Técnica da Ecossistema);
1. Rede - um container rodando **PostgreSQL**;
1. Bolinha - uma estrutura de dados contendo uma mensagem que é aceita pelos serviços *Ping* ou *Pong*;
1. Raquete - um container rodando uma aplicação de controle que irá emitir a mensagem *Bolinha* para *Ping* ou *Pong*.

O funcionamento será o seguinte:

1. Utilizando a *Raquete* emitimos *Bolinha* para *Ping* (por exemplo) usando a *Mesa* como intermediária (*Bolinha* tem que quicar em *Mesa* para a mensagem ser válida), que ao receber a mensagem irá adicionar o texto `Ping` e enviá-la para *Pong* que por sua vez irá gravar um registro na *Rede* contendo a mensagem;
1. No caso da *Raquete* emitir *Bolinha* para *Pong*, este irá adicionar `Pong` à mensagem e enviá-la para *Ping* que então fará a gravação.
